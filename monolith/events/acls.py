import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}"
    headers = { "Authorization" : PEXELS_API_KEY }
    response = requests.get(url, headers=headers)
    text = json.loads(response.content)["photos"]
    photo_url = { "picture_url" : text[1]["src"]["medium"] }
    return photo_url

def get_weather_data(city, state):
    api_key = OPEN_WEATHER_API_KEY
    geocoding_url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={api_key}"
    geo_response = requests.get(geocoding_url)
    geo_content = json.loads(geo_response.content)
    geocoding_lat = geo_content[0]["lat"]
    geocoding_lon = geo_content[0]["lon"]

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={geocoding_lat}&lon={geocoding_lon}&appid={api_key}&units=imperial"
    weather_response = requests.get(weather_url)
    weather_content = json.loads(weather_response.content)
    weather = {
        "temp": weather_content["main"]["temp"],
        "description": weather_content["weather"][0]["description"]
    }
    return weather
